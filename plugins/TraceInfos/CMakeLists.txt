###
#
#  This file is part of the ViTE project.
#
#  This software is governed by the CeCILL-A license under French law
#  and abiding by the rules of distribution of free software. You can
#  use, modify and/or redistribute the software under the terms of the
#  CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
#  URL: "http://www.cecill.info".
#
#    @version 1.2.0
#    @authors COULOMB Kevin
#    @authors FAVERGE Mathieu
#    @authors JAZEIX Johnny
#    @authors LAGRASSE Olivier
#    @authors MARCOUEILLE Jule
#    @authors NOISETTE Pascal
#    @authors REDONDY Arthur
#    @authors VUCHENER Clément
#    @authors RICHART Nicolas
#

set(TRACEINFOS_hdrs
  TraceInfos.hpp
)

set(TRACEINFOS_srcs
  TraceInfos.cpp
)

add_library(TraceInfos SHARED ${TRACEINFOS_srcs})

target_link_libraries(TraceInfos
  Qt5::Widgets
  Qt5::Core
  )

install(TARGETS TraceInfos DESTINATION $ENV{HOME}/.vite)
