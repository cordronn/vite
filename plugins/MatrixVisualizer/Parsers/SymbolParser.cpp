/**
 *
 * @file plugins/MatrixVisualizer/Parsers/SymbolParser.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Arthur Chevalier
 * @author Johnny Jazeix
 *
 * @date 2024-07-17
 */
#include "SymbolParser.hpp"
#include "Readers/Pastix.hpp"

SymbolParser::SymbolParser() {
    m_percentage_loaded = 0;
}

symbol_matrix_t *SymbolParser::parse(const std::string &path, symbol_matrix_t *mtx = nullptr) {
    symbol_matrix_t *matrix;
    FILE *stream;
    int res;

    stream = fopen(path.c_str(), "r");
    if (stream == NULL)
        return nullptr;

    // Parse file (Pastix way for now)
    matrix = new symbol_matrix_t;
    symbol_matrix_init(matrix);

    res = pastix_read_symbol(stream, matrix);

    fclose(stream);

    if (res == 0) {
        return matrix;
    }

    symbol_matrix_deinit(matrix);
    delete matrix;

    return nullptr;
}

float SymbolParser::get_percent_loaded() const {
    return m_percentage_loaded;
}

bool SymbolParser::is_finished() const {
    return m_is_finished;
}

bool SymbolParser::is_cancelled() const {
    return m_is_canceled;
}

void SymbolParser::set_canceled() {
    m_is_canceled = true;
}

void SymbolParser::finish() {
    m_is_finished = true;
}
