###
#
#  This file is part of the ViTE project.
#
#  This software is governed by the CeCILL-A license under French law
#  and abiding by the rules of distribution of free software. You can
#  use, modify and/or redistribute the software under the terms of the
#  CeCILL-A license as circulated by CEA, CNRS and INRIA at the following
#  URL: "http://www.cecill.info".
#
#    @version 1.2.0
#    @authors COULOMB Kevin
#    @authors FAVERGE Mathieu
#    @authors JAZEIX Johnny
#    @authors LAGRASSE Olivier
#    @authors MARCOUEILLE Jule
#    @authors NOISETTE Pascal
#    @authors REDONDY Arthur
#    @authors VUCHENER Clément
#    @authors RICHART Nicolas
#

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC ON)

configure_file(
    "Configuration.in"
    "Configuration.hpp"
)

set(MATRIXVISUALIZER_hdrs
    MatrixVisualizer.hpp
    Configuration.hpp
    Helper.hpp

    Parsers/Parser.hpp
    Parsers/SymbolParser.hpp
    Parsers/OrderParser.hpp
    Parsers/ValuesParser.hpp

    Formats/SymbolMatrix.hpp
    Parsers/Readers/Pastix.hpp

    Windows/MatrixWindow.hpp
    Windows/MatrixGLWidget.hpp

    Common/Zoom.hpp
    Common/Zooming.hpp
    Common/Quadtree.hpp
)

set(MATRIXVISUALIZER_srcs
    MatrixVisualizer.cpp
    Helper.cpp

    Formats/SymbolMatrix.cpp

    Parsers/SymbolParser.cpp
    Parsers/OrderParser.cpp
    Parsers/ValuesParser.cpp
    Parsers/Readers/Pastix.cpp

    Windows/MatrixWindow.cpp
    Windows/MatrixGLWidget.cpp

    Common/Zoom.cpp
    Common/Zooming.cpp
    Common/Quadtree.cpp
)

set(MATRIXVISUALIZER_forms_tmp
    Plugin.ui
)

qt5_wrap_ui(MATRIXVISUALIZER_forms
  ${MATRIXVISUALIZER_forms_tmp}
  )

include_directories(${CMAKE_BINARY_DIR}/plugins/MatrixVisualizer ${CMAKE_CURRENT_SOURCE_DIR})

add_library(MatrixVisualizer SHARED ${MATRIXVISUALIZER_srcs} ${MATRIXVISUALIZER_forms})

#############################################
#              QT5
#############################################
target_link_libraries(MatrixVisualizer
  Qt5::Widgets
  Qt5::Core
  Qt5::Xml
  Qt5::OpenGL
  Qt5::UiTools
  )

target_link_libraries(MatrixVisualizer
  ${OPENGL_gl_LIBRARY}
  ${OPENGL_glu_LIBRARY}
  ${Boost_LIBRARIES}
  )

if(${CMAKE_SYSTEM_NAME} MATCHES "Linux")
  target_link_libraries(MatrixVisualizer
    rt
    )
  add_definitions("-DBOOST_GZIP")
endif()

if(VITE_ENABLE_VBO)
  target_link_libraries(MatrixVisualizer
    ${GLEW_LIBRARY}
    )
endif()

install(TARGETS MatrixVisualizer DESTINATION $ENV{HOME}/.vite)
