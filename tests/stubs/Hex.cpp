/**
 *
 * @file tests/stubs/Hex.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
#include "Hex.hpp"

Hex::Hex(){
    me.erase();
}
std::string Hex::to_string() const{
    return me;
}

bool Hex::instantiate(const std::string &in, Hex &out){
    std::cout << "instantiate " << in << " in hex" << std::endl;
    return true;
}
