/**
 *
 * @file tests/stubs/String.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
#ifndef STRING_HPP
#define STRING_HPP

#include <iostream>
#include "Value.hpp"

/*!
 *
 * \file string.hpp
 * \author  NOISETTE
 * \brief Bouchon
 *
 */
class String : public Value
{
private:
    std::string me;

public:
    String();
    String(const std::string &);
    /*!
     * \fn to_string() const
     */
    std::string to_string() const;

    /*!
     * \fn operator= (const std::string &)
     * \return this String with the new value.
     */
    String operator=(const std::string &);
};

#endif // STRING_HPP
