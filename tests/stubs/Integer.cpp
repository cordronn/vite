/**
 *
 * @file tests/stubs/Integer.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
#include "Integer.hpp"

Integer::Integer(){
    me.erase();
}
std::string Integer::to_string() const{
    return me;
}

bool Integer::instantiate(const std::string &in, Integer &out){
    std::cout << "instantiate " << in << " in integer" << std::endl;
    return true;
}
