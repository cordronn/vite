/**
 *
 * @file tests/stubs/Color.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
#ifndef COLOR_HPP
#define COLOR_HPP

#include <iostream>
#include "Value.hpp"

/*!
 *
 * \file Color.hpp
 * \author  NOISETTE
 * \brief Bouchon
 *
 */

class Color : public Value{
private:
    std::string me;

public:
    Color();

    std::string to_string() const;


    /*!
     *
     * \fn instantiate(const std::string &in, Color &out)
     * \brief Convert a string to a Color
     * \param in String to convert
     * \param out Color to be initialized
     * \return true, if the conversion succeeded
     *
     */
    static bool instantiate(const std::string &in, Color &out);
};


#endif // COLOR_HPP
