/**
 *
 * @file tests/trace/TestParsers.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Olivier Lagrasse
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <vector>
#include <list>
#include <map>

#include "trace/values/Values.hpp"
#include "trace/EntityValue.hpp"
#include "trace/EntityTypes.hpp"
#include "trace/Entitys.hpp"
#include "trace/Trace.hpp"
#include "parser/Parser.hpp"
#include "parser/ParserPaje.hpp"
#include "parser/ParserVite.hpp"


using namespace std;

int main(int argc, char **argv) {

    Parser          *parser = NULL;
    Trace           *trace = NULL;
    vector <string>  filenames;
    string           filename;
    unsigned int     idot; 

    if(argc < 2) {
        cerr << "Usage : " << argv[0]
             << " trace1 trace2 ..." << endl;
        return EXIT_FAILURE;
    }
    else {
        for (int i = 0 ; i < argc ; i ++) {
            filenames.push_back(argv[i]);
        }
    }

    for(int i = 1 ; i < argc ; i ++) {
	trace    = new Trace();
        filename = filenames[i];
        idot     = filename.find_last_of('.');

	std::cout << "Fichier : " << filename << std::endl; 

        if (idot != string::npos) {
            if(filename.substr(idot) == ".trace") {
		std::cout << "Parser  : Paje" << std::endl; 
                parser = new ParserPaje(filename);
            }
#ifdef WITH_OTF
            else if(filename.substr(idot) == ".otf") {
		std::cout << "Parser  : OTF" << std::endl; 
                parser = new ParserOTF(filename);
            }
#endif //WITH_OTF
#ifdef WITH_TAU
            else if(filename.substr(idot) == ".tau") {
		std::cout << "Parser  : TAU" << std::endl; 
                parser = new ParserTAU(filename);
            }
#endif //WITH_OTF
            else if(filename.substr(idot) == ".ept") {
		std::cout << "Parser  : ViTE" << std::endl; 
                parser = new ParserVite(filename);
            }
        }
        if(!parser) { // Default
            std::cout << "Parser  : Problem (Paje)" << std::endl; 
            parser = new ParserPaje(filename);
        }
        
        try {
            parser->parse(*trace);
        }
        catch (const std::string &error) {
            cout << error << "  " << filename << endl;
            parser->finish();
        }

        delete parser;
        parser = NULL;
        delete trace;
        trace = NULL;
    }
    return EXIT_SUCCESS;
}
