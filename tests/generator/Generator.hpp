/**
 *
 * @file tests/generator/Generator.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
#ifndef _VITE_GENERATOR_
#define _VITE_GENERATOR_

#include <QMainWindow>

#include "PajeGenerator.hpp"
#include "TauGenerator.hpp"
#include "OTFGenerator.hpp"

typedef struct _my_link{
    int time;
    int proc;
    int type;
    int value;
    int key;
}my_link;

class Generator : public Writer{

private :
    PajeGenerator* _pajeGen;
    TauGenerator * _tauGen ;
    OTFGenerator * _otfGen ;

    bool _paje;
    bool _tau;
    bool _otf;

    // Values
    QString _name          ; // Name of the file to create
    int     _depth         ; // Depth of the container tree
    int     _procNbr       ; // Number of procs
    int     _numberOfState ; // Number of different states
    int     _numberOfLine  ; // Number of line generated
    bool    _counter       ; // If counter are enabled
    bool    _event         ; // If events are enabled
    bool    _link          ; // If link are enabled
    int     _linkNbr       ; // Number of proc with links
    int     _linkTypeNbr   ; // Number of type of link
    int     _eventNbr      ; // Number of proc with events
    int     _eventTypeNbr  ; // Number of event type
    int     _counterNbr    ; // Number of proc with counters
    int     _numberAction  ; // Number of action available

    // List of started links
    QList<my_link> _startedLink  ;
    int         _size         ; // Number of link started
    int*        _arrayOfValues;

    // Action = changeState, startLink, endLink, event, incCpt, decCpt
    static const int TYPEACTION  = 6;
    static const int CHANGESTATE = 0;
    static const int STARTLINK   = 1;
    static const int ENDLINK     = 2;
    static const int EVENT       = 3;
    static const int INCCPT      = 4;
    static const int DECCPT      = 5;

    int getRand (int maxVal);
    void launchAction (int val, int time);
    my_link getLink (int pos);

public :

    // Constructor
    Generator (QString name,
               int depth, int procNbr, int nbrOfState, int nbrOfLine,
               int countNbr, int eventNbr, int linkNbr,
               int eventTypeNbr, int linkTypeNbr,
               bool paje, bool otf, bool tau);
    // Destructor
    virtual ~Generator     ();
    // Main function that launch the creation of the traces
    void generate  ();

    void initTrace (QString name, int depth, int procNbr, int stateTypeNbr, int eventTypeNbr, int linkTypeNbr, int varNbr);
    void addState  (int proc    , int state, double time);
    void startLink (int proc    , int type , double time);
    void endLink   (int proc    , int type , double time);
    void addEvent  (int proc    , int type , double time);
    void incCpt    (int proc    , int var  , double time);
    void decCpt    (int proc    , int var  , double time);
    void endTrace  ();

};


#endif
