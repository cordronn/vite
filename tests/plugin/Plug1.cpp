/**
 *
 * @file tests/plugin/Plug1.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
#include "Plugin.hpp"

class Plug1 : public Plugin {
public: 
    void init() { printf("Plug1 : init\n"); }
    void set_arguments(std::map<std::string /*argname*/, QVariant */*argValue*/>) {}
    std::string get_name() { return "Plug1"; }

public Q_SLOTS:
    void execute() { printf("Plug1 : exec\n"); }
};

extern "C" {
    Plugin *create() { return new Plug1(); }
}
