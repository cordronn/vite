/**
 *
 * @file src/common/Export.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 *
 * @date 2024-08-02
 */

/*!
 * \file Export.cpp
 * \brief This class defines everything used to export a trace.
 * This can be instantiated and then used to export a file
 * The trace can be exported as: .svg; .png; .jpg; .jpeg
 * It's possible to export only one variable as .txt
 */

#include <ios>
#include <iostream>
#include <fstream>
/* -- */
#include <QFileDialog>
#include <QDialogButtonBox>
/* -- */
#include "ui_list_of_counter_to_export.h"
/* -- */
#include "Export.hpp"
/* -- */
#include "common/Message.hpp"
#include "common/Session.hpp"
/* -- */
#include "render/GanttDiagram.hpp"
#include "render/Render_svg.hpp"
#include "render/RenderLayout.hpp"
/* -- */
#include "trace/DrawTrace.hpp"
#include "trace/values/Date.hpp"
#include "trace/values/Double.hpp"

void Export::export_trace_to_svg(Trace *trace, const std::string &path_to_export, double time_start, double time_end) {
    // Error case when trace is null
    assert(nullptr != trace);

    // Console message depending on the time of start and end of the export
    Message &message = *Message::get_instance();
    message << "export of " << trace->get_filename() << " to " << path_to_export << " between ";
    if (time_start == 0) {
        message << "the beginning of the trace to ";
    }
    else {
        message << time_start << " seconds to ";
    }
    if (time_end != 0) {
        message << time_end << " seconds." << Message::endi;
    }
    else {
        message << "the end of the trace." << Message::endi;
    }

    if (time_end == 0.) {
        time_end = trace->get_max_date().get_value();
    }

    std::cout << "Exporting trace to " << path_to_export << " ... Please Wait " << std::endl;
    Render_svg rsvg(time_start, time_end, trace->get_max_date().get_value(), path_to_export);
    GanttDiagram render(&rsvg);
    DrawTrace drawing_svg;
    drawing_svg.build(&render, trace);
}

void Export::export_trace_to_svg(Trace *trace, const std::string &path_to_export, Interval interval) {
    export_trace_to_svg(trace, path_to_export, interval._left.get_value(), interval._right.get_value());
}

void Export::export_variable(Variable *var, const std::string &filename) {
    const std::list<std::pair<Date, Double>> *variable_values = var->get_values();

    std::ofstream file(filename.c_str(), std::ios::out | std::ios::trunc);

    if (file) {
        double first_value = 0.;
        double second_value = 0.;
        const double min = var->get_min().get_value();
        const double max = var->get_max().get_value();

        for (const auto &variable_value: *variable_values) {
            first_value = variable_value.first.get_value();
            second_value = (variable_value.second.get_value() - min) / (max - min);
            file << first_value << "\t" << second_value << std::endl;
        }

        file.close();
    }
    else {
        *Message::get_instance() << "Unable to open " + filename + " in order to export the counter" << Message::ende;
    }
}

void Export::open_export_window(QWidget *parent, RenderLayout *render_layout) {
    QStringList selected_files;
    QFileDialog file_dialog(parent);
    QString path_to_export;
    QString extension;
    QString export_filename;

    ExportSettings &ES = Session::getSessionExport();

    /* Try to recover the last file dialog state. */
    if (!ES.is_default() && !file_dialog.restoreState(ES.file_dialog_state)) {
        *Message::get_instance() << "Cannot restore the export file dialog state" << Message::endw;
    }

    file_dialog.setLabelText(QFileDialog::FileName, QString::fromStdString(render_layout->get_trace()->get_filename().substr(0, render_layout->get_trace()->get_filename().find_last_of('.'))) + QStringLiteral(".svg"));
    file_dialog.setFileMode(QFileDialog::AnyFile); /* Allow to browse among directories */
    file_dialog.setAcceptMode(QFileDialog::AcceptSave); /* Only save file */
    file_dialog.setNameFilter(QObject::tr("Vector (*.svg);;Bitmap PNG (*.png);;Bitmap JPEG (*.jpeg *.jpg);;Counter (*.txt)"));

    if (file_dialog.exec()) { /* Display the file dialog */
        selected_files = file_dialog.selectedFiles();

        if (selected_files.size() == 1)
            path_to_export = selected_files[0]; /* Get the first filename */
        else {
            if (selected_files.size() == 0)
                *Message::get_instance() << "No file was selected." << Message::endw;
            else
                *Message::get_instance() << "Cannot save: too much files were selected." << Message::endw;

            return;
        }

        std::vector<QString> valid_extension = {
            QStringLiteral("svg"),
            QStringLiteral("png"),
            QStringLiteral("jpeg"),
            QStringLiteral("jpg"),
            QStringLiteral("txt"),
        };

        bool has_valid_extension = false;

        for (int i = 0; i < valid_extension.size(); i++) {
            if (path_to_export.endsWith(QStringLiteral(".") + valid_extension[i])) {
                extension = valid_extension[i];
                export_filename = path_to_export;
                has_valid_extension = true;
                break;
            }
        }

        if (!has_valid_extension) { /* No extension or extension is not correct */
            /* Add it manually */
            const QString filter_selected = file_dialog.selectedNameFilter();

            if (filter_selected == QLatin1String("Vector (*.svg)")) {
                path_to_export += QLatin1String(".svg");
                extension = QStringLiteral("svg");
            }
            else if (filter_selected == QLatin1String("Bitmap PNG (*.png)")) {
                path_to_export += QLatin1String(".png");
                extension = QStringLiteral("png");
            }
            else if (filter_selected == QLatin1String("Bitmap JPEG (*.jpeg *.jpg)")) {
                path_to_export += QLatin1String(".jpg");
                extension = QStringLiteral("jpg");
            }
            else if (filter_selected == QLatin1String("Counter (*.txt)")) {
                path_to_export += QLatin1String(".txt");
                extension = QStringLiteral("txt");
            }
            /* Update the _export_filename attribute which will be used in SVG or Counter export functions */
            export_filename = path_to_export;
        }
    }

    /* Save the file dialog state (History and current directory) */
    ES.file_dialog_state = file_dialog.saveState();

    if (extension == QLatin1String("svg")) {
        export_trace_to_svg(render_layout->get_trace(),
                            path_to_export.toStdString(),
                            render_layout->get_render_area()->get_visible_interval());
    }
    else if (extension == QLatin1String("txt")) {
        // In order to easily retrieve the good variable, we store them with an
        // index representing the container name followed by the variabletype
        // name (because a variable has no name)
        std::map<std::string, Variable *> all_variables;

        render_layout->get_trace()->get_all_variables(all_variables);

        open_export_variable_window(all_variables, export_filename.toStdString());
    }
    else if (extension == QLatin1String("png") || extension == QLatin1String("jpg") || extension == QLatin1String("jpeg")) {

        QImage tempImage = render_layout->get_render_area()->grab_frame_buffer();

        if (!tempImage.save(path_to_export, extension.toUpper().toStdString().c_str())) {
            *Message::get_instance() << "The trace cannot be exported into " + path_to_export.toStdString() + " file." << Message::ende;
        }
        else {
            *Message::get_instance() << "The trace was exported into " + path_to_export.toStdString() + " file." << Message::endi;
        }
    }
}

void Export::open_export_variable_window(const std::map<std::string, Variable *> &variable_list, const std::string &export_filename) {
    Ui_counter_choice_box export_variable_window;
    QDialog dialog_box;
    export_variable_window.setupUi(&dialog_box);

    for (std::map<std::string, Variable *>::const_iterator it = variable_list.begin();
         it != variable_list.end(); ++it) {
        export_variable_window.combobox->addItem(QString::fromStdString((*it).first));
    }

    if (export_variable_window.combobox->count() == 0) {
        *Message::get_instance() << "The trace has no counter" << Message::ende;
        return;
    }

    dialog_box.connect(&dialog_box, &QDialog::accepted, this, [=]() {
        export_variable(variable_list.at(export_variable_window.combobox->currentText().toStdString()), export_filename);
    });

    dialog_box.exec();
}
