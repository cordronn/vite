/**
 *
 * @file src/common/TraceMemory.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
#ifndef TRACEMEMORY_HPP
#define TRACEMEMORY_HPP

typedef enum Trace_State {
    STATE_ALLOC = 27,
    STATE_FREE = 28
} Trace_State_t;

/*
 * Common part :
 *    - FILE          *file   : fichier de trace
 *    - double        time    : timestamp
 *    - INT           procnum
 *    - INT           thrdnum
 *    - Trace_State_T state   : ALLOC or FREE
 *    - INT           memory  : amount of memory allocated or free
 */

#define trace_start(file, time, procnum, thrdnum) \
    fprintf(file, "%9.9lf %ld %ld 0 -1 0\n", (double)time, (long)(procnum), (long)(thrdnum));

#define trace_finish(file, time, procnum, thrdnum) \
    fprintf(file, "%9.9lf %ld %ld 0 -1 1\n", (double)time, (long)(procnum), (long)(thrdnum));

#define trace_malloc(file, time, procnum, state, memory) \
    fprintf(file, "%9.9lf %ld 0 0 4 %ld %f\n",           \
            (double)time, (long)procnum, (long)state, (double)memory / (1 << 20));

#endif /* TRACEMEMORY_H */
