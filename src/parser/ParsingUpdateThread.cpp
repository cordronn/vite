/**
 *
 * @file src/parser/ParsingUpdateThread.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 *
 * @date 2024-07-17
 */

#include <QElapsedTimer>
#include <QThread>
#include <QTimer>
/* -- */
#include "parser/Parser.hpp"
#include "parser/ParsingUpdateThread.hpp"

ParsingUpdateThread::ParsingUpdateThread(Parser *pt, QTimer *update_timer) :
    _parser(pt), _update_timer(update_timer) { }

void ParsingUpdateThread::run() {

    _elapse_timer.start();
    connect(_update_timer, &QTimer::timeout, this, &ParsingUpdateThread::update_slot);
    Q_EMIT update_progress(_elapse_timer.elapsed(), 0);
    exec();
}

void ParsingUpdateThread::update_slot() {
    Q_EMIT update_progress(_elapse_timer.elapsed(), _parser->get_percent_loaded());
}
