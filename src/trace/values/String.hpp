/**
 *
 * @file src/trace/values/String.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Thibault Soucarre
 * @author Clement Vuchener
 *
 * @date 2024-07-17
 */
#ifndef STRING_HPP
#define STRING_HPP

/*!
 *
 * \file String.hpp
 *
 */
/*!
 *
 * \class String
 * \brief Store a string in the trace
 *
 */
class String : public Value
{
private:
    std::string _value;

public:
    /*!
     * \struct less_than
     * \brief Comparator for map
     */
    struct less_than
    {
        /*!
         * \fn operator()(const String &, const String &) const
         * \brief Returns true if the second string is greater than the first one
         */
        bool operator()(const String &, const String &) const;
    };

    /*!
     * \brief Constructor
     */
    String();

    /*!
     * \brief Constructor
     */
    String(std::string);

    /*!
     * \fn to_string() const
     */
    std::string to_string() const override;

    /*!
     * \fn operator ==(const std::string &) const
     * \return true if they are equals.
     */
    bool operator==(const std::string &) const;

    /*!
     * \fn operator= (const std::string &)
     * \return this String with the new value.
     */
    String operator=(const std::string &);
};

#endif // STRING_HPP
