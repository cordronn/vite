/**
 *
 * @file src/trace/values/Integer.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Thibault Soucarre
 * @author Augustin Degomme
 * @author Clement Vuchener
 *
 * @date 2024-07-17
 */
#ifndef INTEGER_HPP
#define INTEGER_HPP

/*!
 *
 * \file Integer.hpp
 *
 */
/*!
 *
 * \class Integer
 * \brief Store a string in the trace
 *
 */
class Integer : public Value
{
private:
    int _value { 0 };

public:
    /*!
     * \brief Constructor
     */
    Integer();

    /*!
     * \brief Constructor
     */
    Integer(int);

    /*!
     * \brief Constructor
     */
    Integer(const std::string &);

    /*!
     *
     * \fn get_value() const
     * \return the value
     *
     */
    int get_value() const { return _value; }

    /*!
     * \fn to_string() const
     * \return a string of the Integer
     */
    std::string to_string() const override;
};

#endif // INTEGER_HPP
