/**
 *
 * @file src/trace/values/Hex.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Thibault Soucarre
 * @author Olivier Lagrasse
 * @author Augustin Degomme
 *
 * @date 2024-07-17
 */
#include <string>
#include <sstream>
#include <cstdio>
#include <iomanip> // For std::setprecision
/* -- */
#include "trace/values/Value.hpp"
#include "trace/values/Hex.hpp"

#if defined WIN32 && !defined(__MINGW32__)
#define sscanf sscanf_s
#endif

Hex::Hex() {
    _is_correct = true;
}

Hex::Hex(unsigned int n) :
    _value(n) {
    _is_correct = true;
}

Hex::Hex(const std::string &in) {
    unsigned int n;
    if (sscanf(in.c_str(), "%X", &n) != 1) {
        _is_correct = false;
    }
    else {
        _value = n;
    }
}

std::string Hex::to_string() const {
    std::ostringstream oss;
    oss.flags(std::ios::hex);
    oss << std::setprecision(Value::_PRECISION) << _value;
    return oss.str();
}
