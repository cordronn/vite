/**
 *
 * @file src/trace/values/Double.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Thibault Soucarre
 * @author Olivier Lagrasse
 * @author Clement Vuchener
 *
 * @date 2024-07-17
 */

#include <string>
#include <iostream>
#include <sstream>
#include <iomanip> // For std::setprecision
/* -- */
#include "common/Tools.hpp"
/* -- */
#include "trace/values/Value.hpp"
#include "trace/values/Double.hpp"

Double::Double() {
    _is_correct = true;
}

Double::Double(double value) :
    _value(value) {
    _is_correct = true;
}

Double::Double(const std::string &value) {
    _is_correct = convert_to_double(value, &_value);
}

std::string Double::to_string() const {
    std::ostringstream oss;
    oss << std::setprecision(Value::_PRECISION) << _value;
    return oss.str();
}

double Double::get_value() const {
    return _value;
}

Double Double::operator+(const Double &d) const {
    return Double(_value + d._value);
}

Double Double::operator-() const {
    return Double(-_value);
}

Double Double::operator-(const Double &d) const {
    return Double(_value - d._value);
}

bool Double::operator<(const Double &d) const {
    return _value < d._value;
}

bool Double::operator>(const Double &d) const {
    return _value > d._value;
}
