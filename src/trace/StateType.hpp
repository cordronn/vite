/**
 *
 * @file src/trace/StateType.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Kevin Coulomb
 * @author Clement Vuchener
 *
 * @date 2024-07-17
 */
#ifndef STATETYPE_HPP
#define STATETYPE_HPP

/*!
 * \file StateType.hpp
 */

#include "trace/EntityType.hpp"

/*!
 * \class StateType
 * \brief Describe the type of states
 */
class StateType : public EntityType
{
private:
public:
    /*!
     * \fn StateType(Name name, ContainerType *container_type, std::map<std::string, Value *> opt)
     * \brief Constructor
     * \param name The name of the state type
     * \param container_type The type of the container that contains the states
     * \param opt Optional parameters
     */
    StateType(Name name, ContainerType *container_type, std::map<std::string, Value *> opt);
    // StateType();
};

#endif
