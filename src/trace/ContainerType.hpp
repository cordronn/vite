/**
 *
 * @file src/trace/ContainerType.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Thibault Soucarre
 * @author Kevin Coulomb
 * @author Olivier Lagrasse
 * @author Clement Vuchener
 *
 * @date 2024-07-17
 */
#ifndef CONTAINERTYPE_HPP
#define CONTAINERTYPE_HPP

/*!
 * \file ContainerType.hpp
 */
/*!
 * \class ContainerType
 * \brief Describe the type of a container
 */
class ContainerType
{
private:
    Name _name;
    ContainerType *_parent;
    std::list<ContainerType *> _children;

public:
    /*!
     * \fn ContainerType(Name &name, ContainerType *parent)
     * \brief Constructor of ContainerType
     * \param name Name of the container type
     * \param parent Type of the parent container
     */
    ContainerType(Name &name, ContainerType *parent);

    /*!
     * \fn ~ContainerType()
     * \brief Destructor
     */
    ~ContainerType();

    /*!
     * \fn add_child(ContainerType *child)
     * \brief Add a type of child container
     * \param child Type of child container
     */
    void add_child(ContainerType *child);

    /*!
     * \fn get_name()
     * \brief name accessor
     */
    const Name get_Name() const;
    const std::string get_name() const;

    /*!
     * \fn get_parent() const
     * \brief Get the parent container type
     */
    const ContainerType *get_parent() const;

    /*!
     * \fn get_children() const
     * \brief Get the list of the child container types
     */
    const std::list<ContainerType *> *get_children() const;
};

#endif
