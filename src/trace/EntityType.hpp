/**
 *
 * @file src/trace/EntityType.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Mathieu Faverge
 * @author Thibault Soucarre
 * @author Kevin Coulomb
 * @author Clement Vuchener
 *
 * @date 2024-07-17
 */
#ifndef ENTITYTYPE_HPP
#define ENTITYTYPE_HPP

/*!
 * \file EntityType.hpp
 */

#include "trace/ContainerType.hpp"

class EntityValue;
/*!
 * \class EntityType
 * \brief A abstract class that represent the various types of entities
 */
class EntityType
{
private:
    EntityClass_t _class; /* Class of EntityType: State, Link, Event or Variable */
    Name _name;
    ContainerType *_container_type;
    std::map<std::string, EntityValue *> _values;
    std::map<std::string, Value *> *_extra_fields;

protected:
    /*!
     * \fn EntityType(Name name, ContainerType *container_type, std::map<std::string, Value *> opt)
     * \brief Default constructor
     * \param name Name of the entity type
     * \param container_type The type of container
     * \param opt Optional fields
     */
    EntityType(EntityClass_t ec, Name name, ContainerType *container_type, std::map<std::string, Value *> &opt);

public:
    // EntityType();

    /*
     * \fn EntityType
     * \brief Destructor
     */
    virtual ~EntityType();

    /*!
     * \fn add_value(EntityValue *value)
     * \brief Add a value for this type of entity
     */
    void add_value(EntityValue *value);

    /*!
     * \fn get_name() const
     * \brief Get the name of the entity type
     */
    const Name get_Name() const;
    const std::string get_name() const;
    const std::string get_alias() const;

    EntityClass_t get_class() const;

    /*!
     * \fn get_container_type() const
     * \brief Get the type of the container
     */
    const ContainerType *get_container_type() const;

    /*!
     * \fn get_values() const
     * \brief Get the list of the values
     */
    const std::map<std::string, EntityValue *> *get_values() const;

    /*!
     * \fn get_extra_fields() const
     * \brief Get the extra fields
     */
    std::map<std::string, Value *> *get_extra_fields() const;

    void set_name(Name name);
    void set_container_type(ContainerType *container_type);
    void set_extra_fields(std::map<std::string, Value *> *extra_fields);
    void set_values(std::map<std::string, EntityValue *> values);
};

#endif
