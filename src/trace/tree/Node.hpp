/**
 *
 * @file src/trace/tree/Node.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Thibault Soucarre
 * @author Kevin Coulomb
 * @author Olivier Lagrasse
 * @author Clement Vuchener
 *
 * @date 2024-07-17
 */
#ifndef NODE_HPP
#define NODE_HPP

/*!
 * \file BinaryTree.hpp
 */

#include "trace/tree/Interval.hpp"
template <typename E>
class BinaryTree;
/*!
 * \class Node
 * \brief Describe a node in a binary tree bound to a element
 */
template <typename E>
class Node
{
    friend class BinaryTree<E>;

private:
    Node<E> *_parent;
    Node<E> *_left_child, *_right_child;

    E *_element;

public:
    /*!
     * \brief Undisplayble interval at the left
     */
    Interval *_left_interval;

    /*!
     * \brief Undisplayble interval at the right
     */
    Interval *_right_interval;

    /*!
     * \brief Constructor
     * \param element Element bound to the node
     */
    Node(E *element) :
        _parent(nullptr), _left_child(nullptr), _right_child(nullptr), _element(element), _left_interval(nullptr), _right_interval(nullptr) {
    }

    /*!
     * \brief Destructor
     */
    ~Node() {
        if (_left_child)
            delete _left_child;
        if (_right_child)
            delete _right_child;
        // delete _element;
        _element = NULL;
        _left_child = NULL;
        _right_child = NULL;
        _parent = NULL;
    }

    /*!
     * \brief Get the parent of the node
     */
    Node<E> *get_parent() const {
        return _parent;
    }

    /*!
     * \brief Get the left child of the node
     */
    Node<E> *get_left_child() const {
        return _left_child;
    }

    /*!
     * \brief Get the right child of the node
     */
    Node<E> *get_right_child() const {
        return _right_child;
    }

    /*!
     * \brief Get the element bound to the node
     */
    const E *get_element() const {
        return _element;
    }
};

#endif
