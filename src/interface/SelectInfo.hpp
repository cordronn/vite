/**
 *
 * @file src/interface/SelectInfo.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 *
 * @date 2024-08-06
 */
/*!
 *\file SelectInfo.hpp
 */

#ifndef SELECT_INFO_HPP
#define SELECT_INFO_HPP

#include <QDialog>
#include <QShowEvent>
#include <QTabWidget>
/* -- */
#include "ui_select_info_window.h"
/* -- */
#include "common/common.hpp"
/* -- */
#include "trace/values/Date.hpp"

class RenderLayout;

/*!
 * \class SelectInfo
 * \brief Class used to view information on a selected Entity
 *
 */

class SelectInfo : public QDialog, protected Ui::select_info_window
{

    Q_OBJECT

private:
    RenderLayout *_render_layout;

    /*!
     * \brief This variable contains a map linking the name of a tab to the date a begin and end of the Entity in the tab
     * This is used to be able to centre on Entity depending on the selected tab
     */
    std::map<const std::string, const std::pair<Element_pos, Element_pos>> _entity_dates;

    /*!
     * \brief Buffer to have access to the dates of the current tab when buttons are pressed
     */
    std::pair<Element_pos, Element_pos> _current_tab_dates;

public:
    /*!
     * Default constructor
     * \param parent The parent widget of the window.
     */
    SelectInfo(RenderLayout *render_layout);

    ~SelectInfo();

    /*!
     * \brief Clear all entity tabs in the select_info_window.
     */
    void clear();

    /*!
     * \brief The function takes strings and/or numbers then displayed it in the entity informative text area in the info window.
     * \param string The string to be displayed.
     */
    void add_info_tab(const std::string &tab_name, const std::string &content, const Element_pos &begin, const Element_pos &end);

    /*!
     * \brief Show the window and the great number of button depending on the typeof entity
     */
    void show_window();

    /*!
     * \brief Set the title of the SelectInfo window
     * \param title the new title
     */
    void set_title(const QString &title);

private Q_SLOTS:
    /*!
     * \brief Update the way buttons are displayed depending on the type of Entity current tab shows
     */
    void on_info_selection_tab_currentChanged();

    /*!
     * \brief Set the start of the Entity to be the center of the view
     */
    void on_center_begin_pressed();

    /*!
     * \brief Set the end of the Entity to be the center of the view
     */
    void on_center_end_pressed();
};

#endif // NODE_SELECT_HPP
