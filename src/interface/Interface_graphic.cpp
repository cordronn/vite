/**
 *
 * @file src/interface/Interface_graphic.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Arthur Redondy
 * @author Mohamed Faycal Boullit
 * @author Mathieu Faverge
 * @author Francois Trahay
 * @author Thibault Soucarre
 * @author Kevin Coulomb
 * @author Philippe Swartvagher
 * @author Olivier Lagrasse
 * @author Augustin Degomme
 *
 * @date 2024-07-17
 */
/*!
 *\file interface_graphic.cpp
 *\brief This is graphical interface C source code.
 */
#include <fstream>
#include <string>
#include <iostream>
#include <sstream>
#include <stack>
#include <map>
#include <list>
/* -- */
#include <QObject>
#include <QWidget>
#include <QtUiTools>/* for the run-time loading .ui file */
#include <QCloseEvent>
#include <QDir>
#include <QFileDialog>
#include <QGLWidget>/* for the OpenGL Widget */
#include <QRadioButton>
#include <QTextEdit>
#include <QCheckBox>
#include <QMessageBox>
#include <QProgressDialog>
/* -- */
#include "common/common.hpp"
#include "common/Errors.hpp"
#include "common/Info.hpp"
#include "common/Tools.hpp"
#include "common/Session.hpp"
/* -- */
#include "trace/values/Values.hpp"
#include "trace/DrawTrace.hpp"
#include "trace/EntityTypes.hpp"
#include "trace/Entitys.hpp"
#include "trace/Trace.hpp"
/* -- */
#include "core/Core.hpp"
/* -- */
#include "interface/Interface_graphic.hpp"
#include "interface/IntervalSelect.hpp"
#include "interface/resource.hpp"
#include "interface/Settings_window.hpp"
/* -- */
#include "plugin/Plugin_window.hpp"
/* -- */
#include "render/GanttDiagram.hpp"
#include "render/RenderLayout.hpp"
#ifdef USE_VULKAN
#include "render/vulkan/Render_vulkan.hpp"
#else
#ifdef VITE_ENABLE_VBO
#include "render/vbo/Render_alternate.hpp"
#else
#include "render/opengl/Render_opengl.hpp"
#endif
#endif

using namespace std;

/***********************************
 *
 *
 *
 * Constructors and destructor.
 *
 *
 *
 **********************************/

Interface_graphic::Interface_graphic(Core *core, QWidget *parent) :
    QMainWindow(parent) {

    setupUi(this);

    _core = core;
    _no_warning = Session::get_hide_warnings_setting(); /* display warnings */

    _reload_type = Session::get_reload_type_setting();
    no_warning->setChecked(Session::get_hide_warnings_setting());
    reload_from_file->setChecked(Session::get_reload_type_setting());
    vertical_line->setChecked(Session::get_vertical_line_setting());
    shaded_states->setChecked(Session::get_shaded_states_setting());

    load_windows();

    setMouseTracking(true); /* to catch mouse events */

    _progress_dialog = nullptr;
    _plugin_window = nullptr;
    _ui_settings = nullptr;
    _ui_node_selection = nullptr;
    _ui_interval_selection = nullptr;

    // For drag and drop operations
    setAcceptDrops(true);
}

Interface_graphic::~Interface_graphic() {
    /* Qt desallocates this, _ui_info_window and _render_area automatically */

    for (RenderLayout *render_layout: _render_layouts) {
        delete render_layout;
    }

    _render_layouts.clear();

    delete _plugin_window;
    _plugin_window = nullptr;
}

/***********************************
 *
 *
 *
 * Window function.
 *
 *
 *
 **********************************/

void Interface_graphic::load_windows() {

    QUiLoader *loader = new QUiLoader();
    QFile file_info(QStringLiteral(":/window/info_window.ui"));
    QFile file_select_info(QStringLiteral(":/window/select_info_window.ui"));

    /* Load the informative window from a .ui file */
    if (file_info.exists()) {
        file_info.open(QFile::ReadOnly);
        CKFP(_ui_info_window = loader->load(&file_info, this), "Cannot open the .ui file: :/window/info_window.ui");
        file_info.close();
    }
    else {
        cerr << __FILE__ << ":" << __LINE__ << ": The following .ui file doesn't exist: :/window/info_window.ui" << endl;
        exit(EXIT_FAILURE);
    }

    /* Set some windows properties */
    _ui_info_window->setWindowFlags(_ui_info_window->windowFlags() | Qt::WindowStaysOnTopHint); /* Always display info_window on top */

    /* Load widget from the .ui file */
    CKFP(_ui_render_area_container_layout = this->findChild<QVBoxLayout *>(QStringLiteral("render_container_layout")), "Cannot find the render_area_container_layout in the .ui file");
    CKFP(_ui_render_area_container_widget = this->findChild<QWidget *>(QStringLiteral("render_container_widget")), "Cannot find the render_area_container_widget in the .ui file");

    CKFP(_ui_fullscreen_menu = this->findChild<QAction *>(QStringLiteral("fullscreen")), "Cannot find the fullscreen menu in the .ui file");
    CKFP(_ui_info_trace_text = _ui_info_window->findChild<QTextEdit *>(QStringLiteral("info_trace_text")), "Cannot find the info_trace_text QTextEdit widget in the .ui file");
    CKFP(_ui_toolbar = this->findChild<QToolBar *>(QStringLiteral("toolBar")), "Cannot find the tool bar in the .ui file");

    CKFP(_ui_recent_files_menu = this->findChild<QMenu *>(QStringLiteral("menuRecent_Files")), "Cannot find the button \"menuRecent_Files\" in the .ui file");

    // For the recent files menu
    for (auto &_recent_file_action: _recent_file_actions) {
        _recent_file_action = nullptr;
    }
    update_recent_files_menu();

    /*
     Special function of Qt which allows methods declared as slots and which
     name are 'on_[widget]_[action]()' to be called when the 'widget' triggered
     the signal corresponding to 'action'.
     /!\ -> use NULL as argument, else messages will be duplicated!
     */
    QMetaObject::connectSlotsByName(nullptr);

    delete loader;
    /* Display the main window */
    this->show();
}

/***********************************
 *
 *
 *
 * Informative message functions.
 *
 *
 *
 **********************************/

void Interface_graphic::error(const string &s) const {
    if (true == _no_warning)
        return; /* do not display error messages */

    QString buf = QString::fromStdString(s);
    _ui_info_trace_text->moveCursor(QTextCursor::End); /* Insert the new text on the end */
    _ui_info_trace_text->insertHtml(QStringLiteral("<font color='red'>") + buf + QStringLiteral("</font><br /><br />"));

    _ui_info_window->show(); /* show info_window */
}

void Interface_graphic::warning(const string &s) const {
    if (true == _no_warning)
        return; /* do not display warning messages */

    QString buf = QString::fromStdString(s);
    _ui_info_trace_text->moveCursor(QTextCursor::End); /* Insert the new text on the end */
    _ui_info_trace_text->insertHtml(QStringLiteral("<font color='orange'>") + buf + QStringLiteral("</font><br /><br />"));

    _ui_info_window->show(); /* show info_window */
}

void Interface_graphic::information(const string &s) const {
    QString buf = QString::fromStdString(s);
    _ui_info_trace_text->moveCursor(QTextCursor::End); /* Insert the new text on the end */
    _ui_info_trace_text->insertHtml(QStringLiteral("<font color='green'>") + buf + QStringLiteral("</font><br /><br />"));
}

/***********************************
 *
 * Widget slot functions.
 *
 **********************************/

void Interface_graphic::on_open_triggered() {
    QString filename = QFileDialog::getOpenFileName(this);

    if (_render_layouts.empty()) {
        open_trace(filename);
        return;
    }

    if (filename.isEmpty()) {
        return;
    }

    // Open the trace in another window
    QStringList arguments = (QStringList() << filename);
    QString program;
    const QString **run_env = _core->get_runenv();
    QDir::setCurrent(*run_env[0]);
    QString run_cmd = *run_env[1];

#ifdef VITE_DEBUG
    cout << __FILE__ << " " << __LINE__ << " : " << run_env[0]->toStdString() << " " << run_env[1]->toStdString() << endl;
#endif

    if (run_cmd.startsWith(QLatin1String(".")))
        program = *run_env[0] + (run_cmd.remove(0, 1));
    else
        program = std::move(run_cmd);

    QProcess new_process;
    new_process.startDetached(program, arguments);
    return;
}

void Interface_graphic::on_reload_triggered() {
    if (!_render_layouts.empty()) {
        Element_pos zoom[2] = { Info::Splitter::_x_min,
                                Info::Splitter::_x_max };
        if (_ui_settings != nullptr) {
            _ui_settings->refresh();
        }
        // if(_ui_node_selection!=NULL)
        //     _ui_node_selection->on_reset_button_clicked();
        // if(_ui_interval_selection!=NULL)
        //  _ui_interval_selection->on_reset_button_clicked();
        if (_reload_type) {
            release_render_area();
        }
        else {
            render_area_clean();
        }
        redraw_trace();

        if (Info::Splitter::_x_max != 0.0) {
            render_zoom_on_interval(zoom[0], zoom[1]);
        }

        get_render_area()->update_render();

        // reset the slider to its original value
        _render_layouts.front()->get_scale_container_slider()->setValue(20);

        // update the interval selection display
        if (_ui_interval_selection != nullptr) {
            _ui_interval_selection->update_interval_layout_from_render_view(_render_layouts.front());
        }
    }
}

void Interface_graphic::open_recent_file() {
    QAction *action = qobject_cast<QAction *>(sender());
    if (action)
        open_trace(action->data().toString());
}

void Interface_graphic::on_clear_recent_files_triggered() {
    Session::clear_recent_files();
    // We remove the elements from the menu
    for (auto &_recent_file_action: _recent_file_actions) {
        if (_recent_file_action != nullptr) {
            _ui_recent_files_menu->removeAction(_recent_file_action);
        }
    }
}

void Interface_graphic::remove_render_area(RenderLayout *render_layout) {

    /*
     * Clear the informative window texts and hide it.
     */
    _ui_info_trace_text->clear(); /* Clear the current text (if exists) */
    _ui_info_window->hide(); /* Hide the informative window */

    if (_ui_settings) {
        _ui_settings->hide();
    }

    if (_plugin_window != nullptr) {
        _plugin_window->hide();
        //_plugin_window->clear_plugins();
    }

    if (nullptr != _ui_interval_selection) {
        _ui_interval_selection->hide();
    }

    // Remove RenderLayout from _render_layouts which is a vector
    for (std::vector<RenderLayout *>::iterator it = _render_layouts.begin(); it != _render_layouts.end(); ++it) {
        if (*it == render_layout) {
            _render_layouts.erase(it);
            break;
        }
    }

    if (_render_layouts.empty()) {
        background_widget->show();
    }

    information(string("File closed."));
}

void Interface_graphic::on_quit_triggered() {

    if (nullptr != _ui_node_selection)
        ((QWidget *)_ui_node_selection)->close();
    if (nullptr != _ui_interval_selection)
        ((QWidget *)_ui_interval_selection)->close();
    if (nullptr != _ui_info_window)
        ((QWidget *)_ui_info_window)->close();
    ((QWidget *)this)->close();
}

void Interface_graphic::on_fullscreen_triggered() {

    /*
     Notice that some problems can appears under X systems with
     the window decoration. Please refer to the Qt official documentation.
     */

    /*
     The menu is checked before function call, so if 'fullscreen menu' is checked,
     the main window is displayed in fullscreen mode
     */

    this->setWindowState(this->windowState() ^ Qt::WindowFullScreen);
}

void Interface_graphic::on_vertical_line_triggered() {
    Info::Render::_vertical_line = !Info::Render::_vertical_line;
    Session::update_vertical_line_setting(Info::Render::_vertical_line);
}

void Interface_graphic::on_shaded_states_triggered() {
    Info::Render::_shaded_states = !Info::Render::_shaded_states;
    Session::update_shaded_states_setting(Info::Render::_shaded_states);

    /* Reload render area */
    if (!_render_layouts.empty()) {
        if (_reload_type)
            release_render_area();
        else
            render_area_clean();
        redraw_trace();
    }
}

void Interface_graphic::on_toolbar_menu_triggered() {
    static bool checked = true; /* Suppose that toolbar_menu is initially checked */

    checked = !checked;

    if (checked)
        _ui_toolbar->show();
    else
        _ui_toolbar->hide();
}

void Interface_graphic::on_minimap_menu_triggered() {
    render_show_minimap();
}

void Interface_graphic::on_show_info_triggered() {
    if (_ui_info_window->isHidden())
        _ui_info_window->show();
    else
        _ui_info_window->hide();
}

void Interface_graphic::on_about_triggered() {

    QMessageBox::about(this, tr("About ViTE"),
                       tr("<h2>ViTE</h2>"
                          "the <b>Vi</b><i>sual </i><b>T</b><i>race</i> <b>E</b><i>xplorer</i> - <i>version " VITE_VERSION
                          "</i> - <i>" VITE_DATE
                          "</i>.<br /><br />"
                          "Under the <a href=\"http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt\">CeCILL A</a> licence."
                          "<br>"
                          "<br>"
                          "Copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria, Univ. Bordeaux. All rights reserved."
                          "<br>"
                          "<br>"
                          "The project main page is:"
                          "<br>"
                          "<a href=\"" VITE_WEBSITE "\">" VITE_WEBSITE
                          "</a>.<br /><br />"));
}

void Interface_graphic::on_documentation_triggered() {
    QDesktopServices::openUrl(QUrl(QString::fromStdString("https://solverstack.gitlabpages.inria.fr/vite/documentation.html")));
}

void Interface_graphic::on_mail_triggered() {
    QDesktopServices::openUrl(QUrl(QString::fromStdString("mailto:vite-issues@inria.fr")));
}
void Interface_graphic::on_show_plugins_triggered() {
    // Don't open the window if no trace is loaded.
    // This avoids crashes when the plugin does not check if the trace is valid
    if (_render_layouts.empty()) {
        return;
    }

    if (_plugin_window == nullptr) { // Creation of the window
        _plugin_window = new Plugin_window(this);
    }

    _plugin_window->update_trace();

    if (get_render_area() != nullptr) {
        _plugin_window->update_render();
    }

    _plugin_window->show();
}

void Interface_graphic::on_show_settings_triggered() {

    if (!_ui_settings) {
        _ui_settings = new Settings_window(this);
        connect(_ui_settings, &Settings_window::settings_changed, this, &Interface_graphic::update_settings);
        // To close the window when we quit the application
        connect(quit, &QAction::triggered, _ui_settings, &QWidget::close);
    }
    _ui_settings->show();
}

void Interface_graphic::on_node_selection_triggered() {

    if (_render_layouts.empty()) {
        error("Must load a trace before using node selection on a trace");
        return;
    }
    if (!_ui_node_selection) {
        _ui_node_selection = new Node_select(this, nullptr);
        connect(quit, &QAction::triggered, _ui_node_selection, &QWidget::close);
    }
    _ui_node_selection->init_window();
    _ui_node_selection->show();
}

void Interface_graphic::on_interval_selection_triggered() {

    if (_render_layouts.empty()) {
        error("Must load a trace before using interval selection on a trace");
        return;
    }
    if (!_ui_interval_selection) {
        _ui_interval_selection = new IntervalSelect(this);
        connect(quit, &QAction::triggered, _ui_interval_selection, &QWidget::close);
    }
    _ui_interval_selection->update_container(_render_layouts);
    for (const RenderLayout *render_layout: _render_layouts) {
        _ui_interval_selection->update_interval_layout_from_render_view(render_layout);
    }
    _ui_interval_selection->show();
}

void Interface_graphic::on_no_warning_triggered() {
    _no_warning = !_no_warning;
    Session::update_hide_warnings_settings(_no_warning);
}

void Interface_graphic::on_no_arrows_triggered() {
    std::cout << "no_arrows triggered" << std::endl;
    Info::Render::_no_arrows = !Info::Render::_no_arrows;
    get_render_area()->update_render();
}

void Interface_graphic::on_no_events_triggered() {
    Info::Render::_no_events = !Info::Render::_no_events;
    get_render_area()->update_render();
}

void Interface_graphic::on_reload_from_file_triggered() {
    _reload_type = !_reload_type;
    Session::update_reload_type_setting(_reload_type);
}

void Interface_graphic::closeEvent(QCloseEvent *event) {
    //   _ui_help_widget->close();
    /* Now, release render */

    if (isEnabled()) {
        event->accept(); /* accept to hide the window for a further destruction */
    }
    else {
        event->ignore();
    }
    if (_ui_settings)
        _ui_settings->close();

    if (_ui_node_selection)
        _ui_node_selection->close();

    if (_ui_interval_selection)
        _ui_interval_selection->close();
}

Core *Interface_graphic::get_console() {
    return _core;
}

Node_select *Interface_graphic::get_node_select() {
    return _ui_node_selection;
}

void Interface_graphic::update_recent_files_menu() {
    const QStringList filenames = Session::get_recent_files();
    QString absoluteFilename;

    for (int i = 0; i < Session::_MAX_NB_RECENT_FILES && i < filenames.size(); ++i) {
        if (_recent_file_actions[i] != nullptr) {
            delete _recent_file_actions[i];
        }
        _recent_file_actions[i] = new QAction(this);
        _recent_file_actions[i]->setVisible(true);
        absoluteFilename = QFileInfo(filenames[i]).absoluteFilePath();

        const QString text = tr("&%1 %2").arg(i + 1).arg(absoluteFilename);
        _recent_file_actions[i]->setText(text);
        _recent_file_actions[i]->setData(absoluteFilename);
        connect(_recent_file_actions[i], &QAction::triggered,
                this, &Interface_graphic::open_recent_file);

        // We add to the menu
        _ui_recent_files_menu->addAction(_recent_file_actions[i]);
    }
}

void Interface_graphic::dragEnterEvent(QDragEnterEvent *event) {
    setBackgroundRole(QPalette::Highlight);
    event->acceptProposedAction();
}

void Interface_graphic::dragMoveEvent(QDragMoveEvent *event) {
    event->acceptProposedAction();
}

void Interface_graphic::dragLeaveEvent(QDragLeaveEvent *event) {
    event->accept();
}

void Interface_graphic::dropEvent(QDropEvent *event) {
    const QMimeData *mimeData = event->mimeData();

    if (!mimeData->hasUrls()) {
        return;
    }
    const QList<QUrl> &urls = event->mimeData()->urls();
    for (const QUrl &url: urls) {
#ifdef WIN32
        const QString filename = url.toString().right(url.toString().size() - 8);
#else
        // We remove file:// from the filename
        const QString filename = url.toString().right(url.toString().size() - 7);
#endif
        open_trace(filename);
    }
}

/* Graphical handling for parsing files */
void Interface_graphic::init_parsing(const std::string &filename) {
    if (!_progress_dialog) {
        _progress_dialog = new QProgressDialog(QObject::tr("Parsing"), QObject::tr("Cancel"), 0, 100, this);
        connect(_progress_dialog, &QProgressDialog::canceled, this, &Interface_graphic::cancel_parsing);
    }
    _progress_dialog->reset();
    _progress_dialog->setWindowTitle(QObject::tr("Loading of ") + QString::fromStdString(filename));
    _progress_dialog->show();
    setDisabled(true); // Disable the main window

    _progress_dialog->setDisabled(false); // to be able to cancel while parsing
}

void Interface_graphic::update_progress_bar(const QString &text, const int loaded) {
    _progress_dialog->setLabelText(text);
    _progress_dialog->setValue(loaded);
    _progress_dialog->update();
    QApplication::processEvents();
}

bool Interface_graphic::is_parsing_canceled() {
    return _progress_dialog->wasCanceled();
}

void Interface_graphic::cancel_parsing() {
    Q_EMIT stop_parsing_event_loop();
}

void Interface_graphic::end_parsing() {
    _progress_dialog->hide();
    _progress_dialog->deleteLater();
    _progress_dialog = nullptr;
    setDisabled(false); // Enable the main window
}

bool Interface_graphic::open_trace(const QString &filepath) {

    // Check if the file has the right permissions
    bool can_open = true;

    if (!QFile::exists(filepath)) {
        error(string("File : ") + filepath.toStdString() + string(" does not exists"));
        can_open = false;
    }
    else if (!(QFile::permissions(filepath) & QFileDevice::ReadUser)) {
        error(string("File : ") + filepath.toStdString() + string(" does not have read permission"));
        can_open = false;
    }

    if (!can_open) {
        Error::set(Error::VITE_ERR_OPEN, Error::VITE_ERRCODE_ERROR);
        Error::print_numbers();
        Error::flush("log.txt", filepath.toStdString());
        return false;
    }

    information(string("File opened: ") + filepath.toStdString());
    Trace *trace = _core->build_trace(filepath.toStdString());

    if (nullptr == trace) {
        on_quit_triggered();
        return false;
    }

    // Add a render area
    add_empty_render_area();

    // The trace needs to be set before a call to draw_trace is done
    // This is because the Render in draw_trace access the trace max_time
    // by getting it from the RenderLayout
    set_trace(trace);
    draw_trace(trace, _render_layouts.size() - 1);

    return true;
}

void Interface_graphic::on_add_trace_triggered() {
    QString filename = QFileDialog::getOpenFileName(this);

    if (filename.isEmpty()) {
        return;
    }

    open_trace(filename);
}

void Interface_graphic::update_settings() {
    // Reload plugins
    if (_plugin_window) {
        _plugin_window->reload_plugins();
    }
    cout << "Settings changed, need to update classes" << endl;

    get_render_area()->update_render();
}

Render_windowed *Interface_graphic::get_render_area() {
    if (_render_layouts.empty()) {
        return nullptr;
    }
    return _render_layouts.front()->get_render_area();
}

void Interface_graphic::render_area_clean() {
    if (get_render_area()->unbuild() == false) {
        std::stringstream stream;
        stream << "(" << __FILE__ << " l." << __LINE__ << "): Close file : an error occurred while cleaning the render.";
        error(stream.str());
    }

    get_render_area()->release();
}

void Interface_graphic::render_show_minimap() {
    get_render_area()->show_minimap();
}

void Interface_graphic::render_zoom_on_interval(const Element_pos &x, const Element_pos &y) {
    get_render_area()->apply_zoom_on_interval(x, y);
}

void Interface_graphic::add_empty_render_area() {
    // If we are adding the first render area, hide the background
    if (_render_layouts.empty()) {
        background_widget->hide();
    }

    Render_windowed *render_area = create_render();

    /* Bind the render area to a RenderLayout */
    RenderLayout *render_layout = new RenderLayout(this, _ui_render_area_container_widget, _ui_render_area_container_layout, render_area);
    _render_layouts.push_back(render_layout);
    render_area->set_layout(render_layout);

    // Wait for initializeGL to be called by the renderer to continue
    render_layout->initialize_gl_widget();
    render_area->update_render();
}

void Interface_graphic::switch_container(const Element_pos &x1, const Element_pos &x2, const Element_pos &y1, const Element_pos &y2) {
    Element_pos yr = y1;
    Element_pos yr2 = y2;
    const Container *container = nullptr;
    const Container *container2 = nullptr;
    const Container::Vector *root_containers = get_trace()->get_view_root_containers();
    if (root_containers->empty())
        root_containers = get_trace()->get_root_containers();
    if (!root_containers->empty()) {
        DrawTrace buf;
        for (const auto &root_container: *root_containers)
            if ((container = buf.search_container_by_position(root_container, yr)))
                break;

        for (const auto &root_container: *root_containers)
            if ((container2 = buf.search_container_by_position(root_container, yr2)))
                break;
    }

    // If the clic is out
    if (!container)
        return;
    if (!container2)
        return;

    // we found the two children containers, we have to know the depth
    Element_pos xr = x1 * (get_trace()->get_depth() + 1);
    if (xr < 0)
        return;

    Element_pos xr2 = x2 * (get_trace()->get_depth() + 1);
    if (xr2 < 0)
        return;
    for (int i = 0; i < (const_cast<Container *>(container)->get_depth() - xr); i++) {
        container = container->get_parent();
    }

    for (int i = 0; i < (const_cast<Container *>(container2)->get_depth() - xr2); i++) {
        container2 = container2->get_parent();
    }

    // we cannot switch when containers' parents are not the same
    if (container->get_parent() != container2->get_parent())
        return;

    if (container == container2)
        return;

    Container *parent = const_cast<Container *>(container->get_parent());

    // printf("we ask to switch %s and %s \n", container->get_name().to_string().c_str(), container2->get_name().to_string().c_str());

    const std::list<Container *> *children = nullptr;
    bool children_allocated = false;

    if (parent == nullptr) { // we switch top level containers
        children = get_trace()->get_view_root_containers();
        if (children->empty())
            children = get_trace()->get_root_containers();
    }
    else {
        children = new std::list<Container *>(*parent->get_view_children());
        if (children->empty()) {
            delete children;
            children = parent->get_children();
        }
        else {
            children_allocated = true;
        }
    }

    std::list<Container *>::const_iterator it = children->begin();
    const std::list<Container *>::const_iterator it_end = children->end();
    if (parent != nullptr) {
        parent->clear_view_children();
        for (; it != it_end; ++it) {
            if ((*it) == container)
                parent->add_view_child(const_cast<Container *>(container2));
            else if ((*it) == container2)
                parent->add_view_child(const_cast<Container *>(container));
            else
                parent->add_view_child(*it);
        }
    }
    else { // for root containers we have to build a new list and fill it
        std::list<Container *> *new_list = new std::list<Container *>();
        for (; it != it_end; ++it) {
            if ((*it) == container) {
                new_list->push_back(const_cast<Container *>(container2));
            }
            else if ((*it) == container2)
                new_list->push_back(const_cast<Container *>(container));
            else
                new_list->push_back(*it);
        }
        get_trace()->set_view_root_containers(*new_list);
        delete new_list;
    }

    redraw_trace();

    if (children_allocated) {
        delete children;
    }
}

Render_windowed *Interface_graphic::create_render() {
    // Check the compatibility of the system with the render engine
#ifdef USE_VULKAN
    return new Render_vulkan(this);
#else

    QGLFormat format(QGL::HasOverlay);

    /* Use compatibility profile for renderText function from QGLWidget, that need deprecated function.
    It should be replaced so we can use Core Profile.
    Version 3.0 is the minimum. If would be better to find a function giving the most recent version available. */
    format.setVersion(3, 0);
    format.setProfile(QGLFormat::CompatibilityProfile);

    if (!QGLFormat::hasOpenGL()) {
        qFatal("This system has no OpenGL support");
    }

#ifdef VITE_ENABLE_VBO
    return new Render_alternate(this, format);
#else
    return new Render_opengl(this, format);
#endif
#endif
}

/***********************************
 *
 * Render area functions.
 *
 **********************************/

void Interface_graphic::release_render_area() {

    if (get_render_area()->unbuild() == false) {
        error("Close file : an error occured with trace releasing.");
    }

    if (_render_layouts.empty()) {
        error("Try to release a render area whereas no trace is loaded");
    }
    else {
        _render_layouts.front()->delete_trace();
    }
    get_render_area()->update_render();
}

void Interface_graphic::redraw_trace() {
    Interval saved_visible_interval = get_render_area()->get_visible_interval();
    render_area_clean();
#if defined(USE_ITC) && defined(BOOST_SERIALIZE)
    get_trace()->updateTrace(saved_visible_interval);
#endif
    draw_trace(get_trace(), 0);
    render_zoom_on_interval(saved_visible_interval._left.get_value(), saved_visible_interval._right.get_value());
}

Trace *Interface_graphic::get_trace() const {
    return _render_layouts.front()->get_trace();
}

void Interface_graphic::set_trace(Trace *trace) {
    _render_layouts.back()->set_trace(trace);
}

void Interface_graphic::update_interval_select_window(const RenderLayout *render_layout) {
    if (nullptr != _ui_interval_selection) {
        _ui_interval_selection->update_interval_layout_from_render_view(render_layout);
    }
}

void Interface_graphic::draw_trace(Trace *trace, const int &index) {

    assert(index < _render_layouts.size());

    // Set application in working state
    QApplication::setOverrideCursor(Qt::WaitCursor);

    DrawTrace drawing_ogl;
    GanttDiagram render(_render_layouts[index]->get_render_area());
    drawing_ogl.build(&render, trace);
    _render_layouts[index]->get_render_area()->build();
    _render_layouts[index]->get_render_area()->refresh_scroll_bars();

    QApplication::restoreOverrideCursor();
}

#include "moc_Interface_graphic.cpp"
