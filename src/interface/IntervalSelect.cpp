/**
 *
 * @file src/interface/IntervalSelect.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Augustin Degomme
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */

#include "interface/IntervalSelect.hpp"

IntervalSelect::IntervalSelect(QWidget *parent) :
    QDialog(parent) {
    setupUi(this);

    // Make the window non resizable
    setFixedSize(this->width(), this->height());
    connect(close_button, &QPushButton::pressed, this, &IntervalSelect::hide);
}

IntervalSelect::~IntervalSelect() = default;

void IntervalSelect::update_container(const std::vector<RenderLayout *> &render_layouts) {

    // Empty interval select layouts
    for (IntervalSelectLayout *interval_select_layout: _interval_select_layouts) {
        interval_select_container_layout->removeWidget(interval_select_layout);
        delete interval_select_layout;
    }
    _interval_select_layouts.clear();

    // Fill interval select layouts with up to date layouts
    for (RenderLayout *render_layout: render_layouts) {
        _interval_select_layouts.push_back(new IntervalSelectLayout(this, render_layout));
        interval_select_container_layout->addWidget(_interval_select_layouts.back());
    }

    // Set window size and button position depending on the number of layouts to display
    int container_height = _interval_select_layouts.size() * 170;

    setFixedHeight(container_height + 150);
    interval_select_container_widget->setFixedHeight(container_height);
    auto_refresh_box->setGeometry(300, container_height + 50, 240, 20);
    reset_all_button->setGeometry(260, container_height + 100, 75, 25);
    apply_button->setGeometry(350, container_height + 100, 75, 25);
    close_button->setGeometry(440, container_height + 100, 75, 25);
}

void IntervalSelect::update_interval_layout_from_render_view(const RenderLayout *render_layout) {
    for (IntervalSelectLayout *child: _interval_select_layouts) {
        if (child->get_render_layout() == render_layout) {
            child->update_interval_select_from_view();
            break;
        }
    }
    apply_button->setEnabled(false);
}

void IntervalSelect::allow_apply() {
    apply_button->setEnabled(true);
}

void IntervalSelect::disable_auto_refresh() {
    auto_refresh_box->setChecked(false);
}

bool IntervalSelect::is_auto_refresh_enabled() {
    return auto_refresh_box->isChecked();
}

void IntervalSelect::on_apply_button_clicked() {
    for (IntervalSelectLayout *child: _interval_select_layouts) {
        child->update_view_from_interval_select();
    }
    apply_button->setEnabled(false);
}

void IntervalSelect::on_reset_all_button_clicked() {
    for (IntervalSelectLayout *child: _interval_select_layouts) {
        child->reset_values();
    }

    if (auto_refresh_box->isChecked()) {
        on_apply_button_clicked();
    }
    else {
        apply_button->setEnabled(true);
    }
}

void IntervalSelect::on_auto_refresh_box_stateChanged() {
    // If the auto_refresh checkbox becomes checked and the apply button is active
    // (meaning that some changes have not been applied)
    // then these changes are applied
    // When there is no pending modification, update_view_from_interval_select is not called
    if (auto_refresh_box->checkState() == Qt::CheckState::Checked && apply_button->isEnabled()) {
        on_apply_button_clicked();
    }
}

#include "moc_IntervalSelect.cpp"
