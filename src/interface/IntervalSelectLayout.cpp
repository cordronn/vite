/**
 *
 * @file src/interface/IntervalSelectLayout.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 *
 * @date 2024-07-30
 */

#include "common/Info.hpp"
/* -- */
#include "interface/IntervalSelect.hpp"
#include "interface/IntervalSelectLayout.hpp"
/* -- */
#include "render/RenderLayout.hpp"
/* -- */
#include "trace/Trace.hpp"

// These are offsets so the trace_end cursor and label are initialy aligned with the slider handle for the max value
#define TRACE_END_CURSOR_X_OFFSET 8
#define TRACE_END_LABEL_X_OFFSET 58
#define TRACE_END_LABEL_Y_OFFSET 40

IntervalSelectLayout::IntervalSelectLayout(IntervalSelect *interval_select, RenderLayout *render_layout) :
    QGroupBox(interval_select), _interval_select(interval_select), _render_layout(std::move(render_layout)) {

    setTitle(QString::fromStdString(_render_layout->get_trace()->get_filename()).split(QString::fromStdString("/")).last() + QString::fromStdString(" "));
    setFixedHeight(150);

    // Initialize spin box labels
    _min_spin_box_label.setParent(this);
    _min_spin_box_label.setAlignment(Qt::AlignRight);
    _min_spin_box_label.setText(QString::fromStdString("From:"));
    _min_spin_box_label.setGeometry(0, 30, 50, 15);

    _max_spin_box_label.setParent(this);
    _max_spin_box_label.setAlignment(Qt::AlignRight);
    _max_spin_box_label.setText(QString::fromStdString("To:"));
    _max_spin_box_label.setGeometry(200, 30, 50, 15);

    // Initialize spin box
    double step = 0.001 * _render_layout->get_trace()->get_max_date().get_value(); // make steps of .1% for spinboxes

    _min_spin_box.setParent(this);
    _min_spin_box.setGeometry(60, 30, 150, 20);
    _min_spin_box.setMinimum(0);
    _min_spin_box.setMaximum(INT32_MAX);
    _min_spin_box.setSingleStep(step);
    connect(&_min_spin_box, static_cast<void (QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), this, &IntervalSelectLayout::on_min_spin_box_value_changed);

    _max_spin_box.setParent(this);
    _max_spin_box.setGeometry(260, 30, 150, 20);
    _max_spin_box.setMinimum(0);
    _max_spin_box.setMaximum(INT32_MAX);
    _max_spin_box.setSingleStep(std::move(step));
    connect(&_max_spin_box, static_cast<void (QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged), this, &IntervalSelectLayout::on_max_spin_box_value_changed);

    // Initialize reset button
    _reset_button.setParent(this);
    _reset_button.setGeometry(420, 30, 75, 20);
    _reset_button.setText(QString::fromStdString("Reset"));
    connect(&_reset_button, &QPushButton::pressed, this, [=]() {        
        reset_values();
        if (_interval_select->is_auto_refresh_enabled()) {
            update_view_from_interval_select();
        }
        else {
            _interval_select->allow_apply();
        } });

    // Initialize range slider labels
    _min_value_range_slider_label.setParent(this);
    _min_value_range_slider_label.setText(QString::number(0));
    _min_value_range_slider_label.setAlignment(Qt::AlignLeft);
    _min_value_range_slider_label.setGeometry(40, 120, 100, 15);

    _max_value_range_slider_label.setParent(this);
    _max_value_range_slider_label.setAlignment(Qt::AlignRight);
    _max_value_range_slider_label.setText(QString::number(_render_layout->get_trace()->get_max_date().get_value()));
    _max_value_range_slider_label.setGeometry(250, 120, 200, 15);

    // Initialize trace end cursor
    _trace_end_cursor.setParent(this);
    _trace_end_cursor.setFrameShape(QFrame::VLine);
    _trace_end_cursor.setStyleSheet(QString::fromStdString("QFrame[frameShape='5']{ border: none; background: #555555;}")); // #555555 : Dark grey
    _trace_end_cursor.setGeometry(_range_slider.geometry().x() + _range_slider.geometry().width() - TRACE_END_CURSOR_X_OFFSET, _range_slider.geometry().y(), 1, 20);

    _trace_end_label.setParent(this);
    _trace_end_label.setText(QString::fromStdString("Trace End\n") + QString::number(_render_layout->get_trace()->get_max_date().get_value()));
    _trace_end_label.setStyleSheet(QString::fromStdString("color : #555555")); // #555555 : Dark grey
    _trace_end_label.setAlignment(Qt::AlignHCenter);
    _trace_end_label.setGeometry(_range_slider.geometry().x() + _range_slider.geometry().width() - TRACE_END_LABEL_X_OFFSET, _range_slider.geometry().y() - TRACE_END_LABEL_Y_OFFSET, 100, 50);

    // Initialise range slider
    _range_slider.setParent(this);
    _range_slider.setGeometry(40, 100, 410, 20);

    // The slider goes from 0 to 1000 for smoother cursor movements
    // The slider values are then converted to trace time values inside the functions
    _range_slider.setMaximum(1000);

    connect(&_range_slider, &RangeSliderWidget::sliderMoved, this, &IntervalSelectLayout::on_sliderMoved);

    _range_span = _range_slider.maximum() - _range_slider.minimum();

    _is_slider_moving = false;
    _max_possible_value = _render_layout->get_trace()->get_max_date().get_value();
}

void IntervalSelectLayout::update_interval_select_from_view() {
    update_values(_render_layout->get_render_area()->get_visible_interval());
}

void IntervalSelectLayout::reset_values() {
    _max_possible_value = _render_layout->get_trace()->get_max_date().get_value();
    _max_value_range_slider_label.setText(QString().setNum(_max_possible_value));
    _trace_end_cursor.move(_range_slider.geometry().x() + _range_slider.geometry().width() - TRACE_END_CURSOR_X_OFFSET, _range_slider.geometry().y());
    _trace_end_label.move(_range_slider.geometry().x() + _range_slider.geometry().width() - TRACE_END_LABEL_X_OFFSET, _range_slider.geometry().y() - TRACE_END_LABEL_Y_OFFSET);
    update_values(Interval(0, _max_possible_value));
}

void IntervalSelectLayout::update_values(const Interval &visible_interval) {

    double lower_bound = visible_interval._left.get_value();
    double higher_bound = visible_interval._right.get_value();

    // choose which value to load the actual position from (can change if we are loading from a splitted file set)
    if (Info::Splitter::preview == true) {
        lower_bound = 0;
        higher_bound = _render_layout->get_trace()->get_max_date().get_value();
    }
    else if (Info::Splitter::load_splitted == true) {
        _interval_select->disable_auto_refresh();
        lower_bound = Info::Splitter::_x_min;
        higher_bound = Info::Splitter::_x_max;
    }

    // Set the values of SpinBox and Slider
    // Block signals to prevent events firing when updating values
    bool v = _min_spin_box.blockSignals(true);
    _min_spin_box.setValue(lower_bound);
    _min_spin_box.blockSignals(v);

    v = _max_spin_box.blockSignals(true);
    _max_spin_box.setValue(higher_bound);
    if (!_is_slider_moving) {
        _max_possible_value = std::max(_max_spin_box.value(), _render_layout->get_trace()->get_max_date().get_value());
        _max_value_range_slider_label.setText(QString().setNum(_max_possible_value));
        _trace_end_cursor.move(_range_slider.geometry().x() + _range_slider.geometry().width() * _render_layout->get_trace()->get_max_date().get_value() / _max_possible_value - TRACE_END_CURSOR_X_OFFSET, _range_slider.geometry().y());
        _trace_end_label.move(_range_slider.geometry().x() + _range_slider.geometry().width() * _render_layout->get_trace()->get_max_date().get_value() / _max_possible_value - TRACE_END_LABEL_X_OFFSET, _range_slider.geometry().y() - TRACE_END_LABEL_Y_OFFSET);
    }
    _max_spin_box.blockSignals(v);

    v = _range_slider.blockSignals(true);
    _range_slider.setLow(int(lower_bound * _range_span / (_max_possible_value - _min_spin_box.minimum())));
    _range_slider.setHigh(int(higher_bound * _range_span / (_max_possible_value - _min_spin_box.minimum())));
    _range_slider.blockSignals(v);
}

const RenderLayout *IntervalSelectLayout::get_render_layout() const {
    return _render_layout;
}

void IntervalSelectLayout::on_min_spin_box_value_changed(double value) {
    /**
     * This method can also change the _max_spin_box value on large trace
     * This is due to scaled float precision error in the apply_zoom_box function (called by update_view_from_interval_select)
     */
    QPalette myPalette(_min_spin_box.palette());

    if (value < _max_spin_box.value()) {
        // Change the value of the slider
        bool v = _range_slider.blockSignals(true);
        _range_slider.setLow(int(value * _range_span / (_max_possible_value - _min_spin_box.minimum())));
        _range_slider.blockSignals(v);

        myPalette.setColor(QPalette::Active, QPalette::Text, Qt::black);
        myPalette.setColor(QPalette::Active, QPalette::HighlightedText, Qt::white);

        if (_interval_select->is_auto_refresh_enabled()) {
            update_view_from_interval_select();
        }
        else {
            _interval_select->allow_apply();
        }
    }
    else {
        // Set the text to red if min >= max
        myPalette.setColor(QPalette::Active, QPalette::Text, Qt::red);
        myPalette.setColor(QPalette::Active, QPalette::HighlightedText, Qt::red);
    }

    _min_spin_box.setPalette(myPalette);
    _max_spin_box.setPalette(myPalette);
}

void IntervalSelectLayout::on_max_spin_box_value_changed(double value) {
    /**
     * This method can also change the _min_spin_box value on large trace
     * This is due to scaled float precision error in the apply_zoom_box function (called by update_view_from_interval_select)
     */
    QPalette myPalette(_min_spin_box.palette());

    if (value > _min_spin_box.value()) {

        _max_possible_value = std::max(_max_spin_box.value(), _render_layout->get_trace()->get_max_date().get_value());
        _max_value_range_slider_label.setText(QString().setNum(_max_possible_value));
        _trace_end_cursor.move(_range_slider.geometry().x() + _range_slider.geometry().width() * _render_layout->get_trace()->get_max_date().get_value() / _max_possible_value - TRACE_END_CURSOR_X_OFFSET, _range_slider.geometry().y());
        _trace_end_label.move(_range_slider.geometry().x() + _range_slider.geometry().width() * _render_layout->get_trace()->get_max_date().get_value() / _max_possible_value - TRACE_END_LABEL_X_OFFSET, _range_slider.geometry().y() - TRACE_END_LABEL_Y_OFFSET);

        bool v = _range_slider.blockSignals(true);
        _range_slider.setHigh(int(value * _range_span / (_max_possible_value - _max_spin_box.minimum())));
        _range_slider.blockSignals(v);

        myPalette.setColor(QPalette::Active, QPalette::Text, Qt::black);
        myPalette.setColor(QPalette::Active, QPalette::HighlightedText, Qt::white);

        if (_interval_select->is_auto_refresh_enabled()) {
            update_view_from_interval_select();
        }
        else {
            _interval_select->allow_apply();
        }
    }
    else {
        // Set the text to red if max <= min
        myPalette.setColor(QPalette::Active, QPalette::Text, Qt::red);
        myPalette.setColor(QPalette::Active, QPalette::HighlightedText, Qt::red);
    }

    _min_spin_box.setPalette(myPalette);
    _max_spin_box.setPalette(myPalette);
}

void IntervalSelectLayout::on_sliderMoved(int low, int high) {
    // If the SpinBox value is changed directly it will launch a valueChanged signal
    // thus changing the value of the slider creating an infinite loop
    // The signal is blocked to prevent these infinite loops
    bool v = _min_spin_box.blockSignals(true);
    _min_spin_box.setValue(low * (_max_possible_value - _min_spin_box.minimum()) / _range_span);
    _min_spin_box.blockSignals(v);

    v = _max_spin_box.blockSignals(true);
    _max_spin_box.setValue(high * (_max_possible_value - _max_spin_box.minimum()) / _range_span);
    _max_spin_box.blockSignals(v);

    if (_interval_select->is_auto_refresh_enabled()) {
        // update_view_from_interval_select refreshes the scroll bar which calls update_values
        // This is because we want to update the values when the scroll bar is changed by the user
        // But this changes _max_possible_value
        // A bool is then set so _max_possible_value is not changed when slider is moved
        _is_slider_moving = true;
        update_view_from_interval_select();
        _is_slider_moving = false;
        return;
    }

    _interval_select->allow_apply();
}

void IntervalSelectLayout::update_view_from_interval_select() {

    if (_min_spin_box.value() != _max_spin_box.value()) {

        // reload data from disk if needed
#if defined(USE_ITC) && defined(BOOST_SERIALIZE)
        Info::Splitter::_x_min = _min_spin_box.value();
        Info::Splitter::_x_max = _max_spin_box.value();
        Interval visible_interval(_min_spin_box.value(), _max_spin_box.value());
        _render_layout->get_trace()->updateTrace(std::move(visible_interval));
#endif

        _render_layout->get_render_area()->apply_zoom_on_interval(_min_spin_box.value(), _max_spin_box.value());
    }
}

#include "moc_IntervalSelectLayout.cpp"
