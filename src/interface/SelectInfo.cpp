/**
 *
 * @file src/interface/SelectInfo.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 *
 * @date 2024-08-06
 */
/*!
 *\file SelectInfo.cpp
 *\brief This file defines a class for the window popping up when the user double click on an entity
 * This window displays information on the Entity clicked
 * When several Entities are clicked on at the same time, information for each are displayed in tabs
 */

#include "interface/SelectInfo.hpp"
/* -- */
#include "render/RenderLayout.hpp"
/* -- */
#include <QTextEdit>

SelectInfo::SelectInfo(RenderLayout *render_layout) :
    QDialog(nullptr), _render_layout(render_layout) {
    setupUi(this);
}

SelectInfo::~SelectInfo() = default;

void SelectInfo::clear() {
    info_selection_tab->clear(); /* Remove all tabs in the QTabWidget*/
    _entity_dates.clear();
}

void SelectInfo::add_info_tab(const std::string &tab_name, const std::string &content, const Element_pos &begin, const Element_pos &end) {
    // Create and setup the tab
    QTextEdit *text_widget = new QTextEdit(info_selection_tab);
    text_widget->setReadOnly(true);
    text_widget->setTextInteractionFlags(Qt::TextSelectableByMouse | Qt::TextSelectableByKeyboard);

    // Fill the text content and add the tab to the window
    text_widget->insertHtml(QStringLiteral("<font color='blue'>") + QString::fromStdString(content) + QStringLiteral("</font>"));
    text_widget->moveCursor(QTextCursor::Start); // Make the text edit scroll to the top
    info_selection_tab->addTab(text_widget, QString::fromStdString(tab_name));

    // The name of the widget is set to tab_name because the text of the label can't be accessed
    // This name needs to be accessible to know on which entity to center if the buttons were to be pressed
    text_widget->setObjectName(QString::fromStdString(tab_name));

    // Pair linking a begin time with an end time
    const std::pair<Element_pos, Element_pos> dates(begin, end);
    // Pair linking a tab_name and a couple of dates
    const std::pair<std::string, std::pair<double, double>> entity_dates_pair(tab_name, std::move(dates));
    // Add the pair (tab_name, dates) to a map
    _entity_dates.insert(std::move(entity_dates_pair));
}

void SelectInfo::set_title(const QString &title) {
    // Spaces are added at the end otherwise the text can be clipped depending on the default font (under Debian for example)
    this->setWindowTitle(title + QString::fromStdString("     "));
}

void SelectInfo::on_info_selection_tab_currentChanged() {
    // If there is no tab, hide the buttons
    if (info_selection_tab->count() == 0) {
        center_begin->hide();
        center_end->hide();
        return;
    }

    _current_tab_dates = _entity_dates[info_selection_tab->currentWidget()->objectName().toStdString()];

    // When the dates of start and finish are the same (for Event and Variable) only show one button
    if (_current_tab_dates.first == _current_tab_dates.second) {
        center_begin->setText(QString::fromStdString("Center on ") + info_selection_tab->currentWidget()->objectName());
        center_begin->show();
        center_end->hide();
        return;
    }

    // For State and Link Entity show every buttons
    center_begin->setText(QString::fromStdString("Center on ") + info_selection_tab->currentWidget()->objectName() + QString::fromStdString(" Start"));
    center_begin->show();

    center_end->setText(QString::fromStdString("Center on ") + info_selection_tab->currentWidget()->objectName() + QString::fromStdString(" End"));
    center_end->show();
}

void SelectInfo::show_window() {
    on_info_selection_tab_currentChanged();
    show();
}

void SelectInfo::on_center_begin_pressed() {
    _render_layout->get_render_area()->set_center_on(_current_tab_dates.first);
}

void SelectInfo::on_center_end_pressed() {
    _render_layout->get_render_area()->set_center_on(_current_tab_dates.second);
}
