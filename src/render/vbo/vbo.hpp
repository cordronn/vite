/**
 *
 * @file src/render/vbo/vbo.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Thibault Soucarre
 * @author Johnny Jazeix
 * @author Olivier Lagrasse
 *
 * @date 2024-07-17
 */
/*!
 *\file vbo.hpp
 */

#ifndef VBO_HPP
#define VBO_HPP
#include <vector>
#include "common/common.hpp"
using namespace std;

// macro used by OpenGL
#define BUFFER_OFFSET(a) ((char *)NULL + (a))
class Shader;
/*!
 * \brief Manage the Vertex Buffer Object.
 */
class Vbo
{

private:
    GLuint _vboID;
    GLuint _vaoID;
    int _nbVertex;
    Shader *_shader;
    vector<Element_pos> _vertex;
    vector<Element_col> _colors;
    vector<char> _shaded;
    vector<float> _shaded2;
    vector<Element_pos> _texture_coord;

public:
    /***********************************
     *
     * Constructor and destructor.
     *
     **********************************/

    /*!
     * \brief Constructor.
     */
    Vbo();

    /*!
     * \brief Constructor.
     * \param Shader the shader we use for the entity type the vbo stand for
     */
    Vbo(Shader *s);

    /*!
     * \brief The destructor.
     */
    virtual ~Vbo();

    /***********************************
     *
     * Buffer filling.
     *
     **********************************/

    /*!
     * \brief Add vertex to a vbo storing coordinates and colors
     */
    int add(Element_pos x, Element_pos y, Element_col r, Element_col g, Element_col b);
    /*!
     * \brief Add vertex to a vbo storing coordinates and textures
     */
    int add(Element_pos x, Element_pos y, Element_pos tx, Element_pos ty);
    /*!
     * \brief Add vertex to a state vbo using char for color gradient (glsl >= 330)
     */
    int add(Element_pos x, Element_pos y, char b);
    /*!
     * \brief Add vertex to a state vbo using float for color gradient (glsl < 330)
     */
    int add(Element_pos x, Element_pos y, float b);
    /*!
     \brief Add vertex to a link/event vbo
     */
    int add(Element_pos x, Element_pos y);
    /*!
     * \brief Should be called before using vbo in paintGL
     */
    void lock();
    /*!
     * \brief Should be called after using vbo in paintGL
     */
    void unlock();
    int getNbVertex();
    void setNbVertex(int);
    /*!
     * \brief Configuration function. Create VBO and VAO and send datas to GPU
     */
    void config(int glsl);
    Shader *get_shader();
    /*
     * \brief delete current used shader
     */
    void delete_shader();
    /*
     * \brief Change Shader used with this vbo
     */
    void set_shader(Shader *s);
};

#endif
