/**
 *
 * @file src/render/vulkan/Vk_vertex_buffer.cpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Lucas Guedon
 *
 * @date 2024-07-17
 */
#include "render/vulkan/Vk_vertex_buffer.hpp"

Vk_vertex_buffer::Vk_vertex_buffer::Vk_vertex_buffer() :
    Vk_buffer(VK_BUFFER_USAGE_VERTEX_BUFFER_BIT) { }

uint32_t Vk_vertex_buffer::vertex_count() {
    return _vertex_count;
}

void Vk_vertex_buffer::bind_vertex_buffer(VkCommandBuffer *command_buffer) {
    VkDeviceSize vb_offset = 0;
    _dev_funcs->vkCmdBindVertexBuffers(*command_buffer, 0, 1, &_buffer, &vb_offset);
}

void Vk_vertex_buffer::draw(VkCommandBuffer *command_buffer) {
    if (_vertex_count > 0) {
        _dev_funcs->vkCmdDraw(*command_buffer, _vertex_count, 1, 0, 0);
    }
}