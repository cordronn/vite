/**
 *
 * @file src/render/Geometry.hpp
 *
 * @copyright 2008-2024 Bordeaux INP, CNRS (LaBRI UMR 5800), Inria,
 *                      Univ. Bordeaux. All rights reserved.
 *
 * @author Camille Ordronneau
 * @author Johnny Jazeix
 * @author Olivier Lagrasse
 * @author Mathieu Faverge
 *
 * @date 2024-07-17
 */
/*!
 *\file Geometry.hpp
 */

#ifndef GEOMETRY_HPP
#define GEOMETRY_HPP

/*!
 * \brief This class provide some tools to manage the window and render area geometry.
 */
class Geometry
{

private:
    /**
     * \brief Length of the trace on which Geometry works on
     * Used for coeff_trace_render_x to convert trace x coordinate into render x coordinate
     */
    float _trace_length;

protected:
    /*!
     * \brief The last x position of the point (for counter).
     */
    Element_pos _counter_last_x;

    /*!
     * \brief The last y position of the point (for counter).
     */
    Element_pos _counter_last_y;

    /*!
     * \brief The opengl render area width in pixels.
     */
    //  Element_pos _screen_width;

    /*!
     * \brief The opengl render area height in pixels.
     */
    //    Element_pos _screen_height;

    /*!
     * \brief The opengl visibled scene width in the OpenGL units.
     */
    //  Element_pos _render_width;

    /*!
     * \brief The opengl visibled scene height in the OpenGL units.
     */
    //    Element_pos _render_height;

    /*!
     * \brief The width of container area draw.
     */
    //  Element_pos _container_x_max;

    /*!
     * \brief The height of container area draw.
     */
    //    Element_pos _container_y_max;

    /*!
     * \brief The x base of container area draw.
     */
    // Element_pos _container_x_min;

    /*!
     * \brief The y base of container area draw.
     */
    // Element_pos _container_y_min;

    /*!
     * \brief The width of state area draw.
     */
    // Element_pos _state_x_max;

    /*!
     * \brief The height of state area draw.
     */
    Element_pos _state_y_max;

    /*!
     * \brief The x base of state area draw.
     */
    // Element_pos _state_x_min;

    /*!
     * \brief The y base of state area draw.
     */
    Element_pos _state_y_min;

    /*!
     * \brief z position for the ruler.
     */
    Element_pos _z_ruler;

    /*!
     * \brief z position for objects over the ruler.
     */
    Element_pos _z_ruler_over;

    /*!
     * \brief z position for objects under the ruler.
     */
    Element_pos _z_ruler_under;

    /*!
     * \brief z position for containers.
     */
    Element_pos _z_container;

    /*!
     * \brief z position for objects under containers.
     */
    Element_pos _z_container_under;

    /*!
     * z position for states.
     */
    Element_pos _z_state;

    /*!
     * z position for events.
     */
    Element_pos _z_event;

    /*!
     * z position for arrows.
     */
    Element_pos _z_arrow;

    /*!
     * z position for counters.
     */
    Element_pos _z_counter;

    /*!
     * Default offset of entities drawing.
     */
    Element_pos _default_entity_x_translate;

    /*!
     * Distance between two ruler measures.
     */
    Element_pos _ruler_distance;

    /*!
     * Height of the ruler.
     */
    Element_pos _ruler_height;

    /*!
     *  Highness of the ruler.
     */
    Element_pos _ruler_y;

    /*!
     * \brief The percentage taken by container display in the render area.
     */
    Element_pos _x_scale_container_state;

    /*!
     * \brief the x scale of state drawing.
     */
    Element_pos _x_state_scale;

    /*!
     * \brief the y scale of state drawing.
     */
    Element_pos _y_state_scale;

    /*!
     * \brief The x position of camera view for state drawing area.
     */
    Element_pos _x_state_translate;

    /*!
     * \brief The y position of camera view for state drawing area.
     */
    Element_pos _y_state_translate;

    /*!
     * \brief The minimum visible time of the trace
     */
    Element_pos _min_visible_time;

    /*!
     * \brief The maximum visible time of the trace
     */
    Element_pos _max_visible_time;

    /**
     * \brief Height of the trace on which Geometry works on
     * Used for coeff_trace_render_y to convert trace y coordinate into render y coordinate
     */
    Element_pos _trace_height;

    /***********************************
     *
     * Constructor and destructor.
     *
     **********************************/
public:
    /*!
     * \brief The constructor.
     */
    Geometry();

    /*!
     * \brief The destructor
     */
    virtual ~Geometry();

    /***********************************
     *
     * Init function.
     *
     **********************************/

    /*!
     * \brief Initialize geometry attributes.
     */
    void init_geometry();

    /**
     * \brief Setter for _trace_length
     * \param new_trace_length New trace length in trace time
     */
    void set_trace_length(float new_trace_length);

    /***********************************
     *
     * Coordinate convert functions.
     *
     **********************************/

    /*!
     * \brief This function convert a X screen coordinate into render coordinate.
     * \param e A X screen coordinate.
     * \return A X render coordinate corresponding to e but in render area coordinate.
     */
    Element_pos screen_to_render_x(Element_pos e) const;

    /*!
     * \brief This function convert a Y screen coordinate into render coordinate.
     * \param e A Y screen coordinate.
     * \return A Y render coordinate corresponding to e but in render area coordinate.
     */
    Element_pos screen_to_render_y(Element_pos e) const;

    /*!
     * \brief This function convert a X render coordinate into trace coordinate.
     * \param e A X render coordinate.
     * \return A X trace coordinate corresponding to e but in trace coordinate.
     */
    Element_pos render_to_trace_x(Element_pos e) const;

    /*!
     * \brief This function convert a Y render coordinate into trace coordinate.
     * \param e A Y render coordinate.
     * \return A Y trace coordinate corresponding to e but in trace coordinate.
     */
    Element_pos render_to_trace_y(Element_pos e) const;

    /*!
     * \brief Dual of screen_to_render_x.
     * \param e A X render coordinate.
     * \return A X screen coordinate corresponding to e but in screen coordinate.
     */
    Element_pos render_to_screen_x(Element_pos e) const;

    /*!
     * \brief Dual of screen_to_render_y.
     * \param e A Y render coordinate.
     * \return A Y screen coordinate corresponding to e but in screen coordinate.
     */
    Element_pos render_to_screen_y(Element_pos e) const;

    /*!
     * \brief Dual of render_to_trace_x.
     * \param e A X trace coordinate.
     * \return A X render coordinate corresponding to e but in render area coordinate.
     */
    Element_pos trace_to_render_x(Element_pos e) const;

    /*!
     * \brief  Dual of render_to_trace_y.
     * \param e A Y trace coordinate.
     * \return A Y render coordinate corresponding to e but in render area coordinate.
     */
    Element_pos trace_to_render_y(Element_pos e) const;

    /*!
     * \brief  Return the coefficient used to convert from horizontal screen scale to the horizontal render scale by multiplication.
     * \return A number with which an horizontal screen width number must be multiply to get its horizontal render width.
     */
    Element_pos coeff_screen_render_x() const;

    /*!
     * \brief  Return the coefficient used to convert from vertical screen scale to the vertical render scale by multiplication.
     * \return A number with which a vertical screen height number must be multiply to get its vertical render height.
     */
    Element_pos coeff_screen_render_y() const;

    /*!
     * \brief  Return the coefficient used to convert from horizontal trace scale to the horizontal render scale by multiplication.
     * \return A number with which an horizontal trace width number must be multiply to get its horizontal render width.
     */
    Element_pos coeff_trace_render_x() const;

    /*!
     * \brief  Return the coefficient used to convert from vertical trace scale to the vertical render scale by multiplication.
     * \return A number with which a vertical trace height number must be multiply to get its vertical render height.
     */
    Element_pos coeff_trace_render_y() const;

    /*!
     * \brief This function updates the visible interval value.
     */
    void update_visible_interval_value();

    /* Return the coordinates of the current view (in screen coordinate) */
    /*   Element_pos visible_screen_x_min();

    Element_pos visible_screen_x_max();

    Element_pos visible_screen_y_min();

    Element_pos visible_screen_y_max();*/
};

#endif
