
This is ViTE version 1.4.

ViTE is a C++ Visual Trace Explorer using Qt for viewing.



# Installing ViTE

## Using Windows installer

Run the installer provided.

## Using Debian/Ubuntu package manager

Since ViTE is already in Debian official repository, you can install the stable version with:
```
sudo apt-get install vite
```

If you prefer to rebuild it from scratch, install all required dependencies with:
```
sudo apt-get build-dep vite
```

## Using Spack installer

Building ViTE from source without root permission (eg. on a cluster)
can be tedious because of its dependencies (eg. QT). A spack package
from ViTE automates the compilation of ViTE and its dependencies. This
package comes with two *variants*: `otf2` (to enable support for OTF2
traces) and `tau` (to enable support for Tau traces).

```
spack install vite
or
spack install vite+otf2
```

## Building from source

### Dependencies

If you do not have Qt please visit: https://www.qt.io/ and download the development files for your operating system.

For Debian users, you may need to install several packages:
```
apt-get install qtbase5-dev qt5-qmake qttools5-dev libqt5charts5-dev libqt5svg5-dev
apt-get install libglm-dev libglew-dev
```

### Compiling ViTE
Then, after Qt development files have been installed, you can compile ViTE:

```
cmake -DCMAKE_INSTALL_PREFIX:PATH=. .
```
or
```
ccmake
```
(which allows you to choose the directories and library for tau, otf2, etc.)

will configure your Makefile depending on your OS.
You can also use one of the `config.XXXX` scripts (`./config.XXXX`) to configure cmake.

Then, type the command:
```
make
```
to create the binary file.

## Using Fedora/CentOS package

You have to have the administration rights (su).
you will need to get the following rpm packages:
```
wget ftp://ftp.pbone.net/mirror/ftp.pramberger.at/systems/linux/contrib/rhel5/i386/qt45-4.5.1-1.el5.pp.i386.rpm
wget ftp://ftp.pbone.net/mirror/ftp.pramberger.at/systems/linux/contrib/rhel5/i386/qt45-devel-4.5.1-1.el5.pp.i386.rpm
wget ftp://ftp.pbone.net/mirror/ftp.pramberger.at/systems/linux/contrib/rhel5/i386/qt45-sqlite-4.5.1-1.el5.pp.i386.rpm
```

Then, you will need to install the required packages used by the previous ones:
```
yum install dbus-devel fontconfig-devel freetype-devel glib2-devel libSM-devel libX11-devel libXcursor-devel libXext-devel libXfixes-devel libXi-devel libXinerama-devel libXrandr-devel libXrender-devel libpng-devel mesa-libGLU-devel glut glut-devel gcc-c++
```

After you can install the rpm packages you just download:
```
rpm -i qt45-4.5.1-1.el5.pp.i386.rpm
rpm -i qt45-sqlite-4.5.1-1.el5.pp.i386.rpm
rpm -i qt45-devel-4.5.1-1.el5.pp.i386.rpm
```

You have to export your `PATH` variable to the directory of qmake, which is by default:
```
export PATH=/usr/lib/qt45/qt/bin:/usr/lib/qt45/qt/include:$PATH
```

If it is not the case, try: `find / | grep qmake` and take the path given.

Go to the `vite/src/trunk` directory then:
```
./configure
make
```



# Documentation

For ViTE there is three documentations that can be found in the `docs` directory:

- `user_manual`: the user manual that explains ViTE and its features.
- `technical_manual`: the technical manual explains how ViTE is implemented
- `doxygen`: the Doxygen explains the classes and functions more in details.

For Doxygen, it is necessary to have `epstopdf` for the latex version. You can install it with the package `texlive-extra-utils`.
Also, install the `dot` program for graphics from the package `graphviz`.

The doxygen documentation can be compiled by the command: `make doc`.

# Supported Platforms

For this release, the following platforms have been tested on:
- Ubuntu 8.04 / 8.10 / 9.04 / 10.04.
- Windows Xp/Vista.
- Mandriva linux one 2009 kde (under virtualbox).
- MacOS X.

# Reporting bugs


To report a bug, if you have an Inria account, submit an issue on ViTE gitlab project:
https://gitlab.inria.fr/solverstack/vite/-/issues

Otherwise, please report the issue to vite-issues@inria.fr
